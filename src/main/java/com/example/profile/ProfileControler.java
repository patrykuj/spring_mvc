package com.example.profile;

/**
 * Created by kate on 2016-08-20.
 */

import com.example.dateFormatter.DateFormatter;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URLConnection;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class ProfileControler {
    private final static Logger LOGGER = Logger.getLogger(ProfileControler.class.getName());
    private final UserProfileSession userProfileSession;

    @Autowired
    public ProfileControler(UserProfileSession userProfileSession){
        this.userProfileSession = userProfileSession;
    }

    @RequestMapping("/profile")
    public String displayProfile(ProfileForm profileForm){
        return "profile/profilePage";
    }

    @RequestMapping(value = "/profile", params = {"save"}, method = RequestMethod.POST)
    public String saveProfile(@Valid ProfileForm profileForm, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            LOGGER.log(Level.ALL, "profileForm has errors");
            return "profile/profilePage";
        }
        userProfileSession.saveForm(profileForm);
        LOGGER.log(Level.ALL, "profile saved: " + profileForm.getEmail() + " " + profileForm.getTwitterHandle() + " " + profileForm.getBirthDate());
        return "redirect:/search/mixed;keywords=" + String.join(",",profileForm.getTastes());
    }

    @RequestMapping(value="/profile", params = {"addTaste"})
    public String addRow(ProfileForm profileForm){
        profileForm.getTastes().add(null);
        return "profile/profilePage";
    }

    @RequestMapping(value="/profile", params = {"removeTaste"})
    public String removeRow(ProfileForm profileForm, HttpServletRequest req){
        Integer rowId = Integer.valueOf(req.getParameter("removeTaste"));
        profileForm.getTastes().remove(rowId.intValue());
        return "profile/profilePage";
    }

    @ModelAttribute("dateFormat")
    public String localeFormat(Locale locale){
        return DateFormatter.getPattern(locale);
    }

    @ModelAttribute
    public ProfileForm toForm(){
        return userProfileSession.toForm();
    }
}
