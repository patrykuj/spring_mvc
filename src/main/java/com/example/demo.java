package com.example;

import com.example.Config.PictureUploadProperties;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by epakarb on 2016-08-28.
 */
@SpringBootApplication
@EnableConfigurationProperties({PictureUploadProperties.class})
public class demo extends WebMvcConfigurerAdapter{

}
