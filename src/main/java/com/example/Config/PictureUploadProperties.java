package com.example.Config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;

/**
 * Created by epakarb on 2016-08-28.
 */
@ConfigurationProperties(prefix = "upload.pictures")
public class PictureUploadProperties {
    private Resource uploadPath;
    private Resource anonymousPicture;

    public Resource getAnonymousPicture(){
        return anonymousPicture;
    }

    public void setAnonymousPicture(String pathToAnnonymousPicture){
        this.anonymousPicture = new DefaultResourceLoader().getResource(pathToAnnonymousPicture);
    }

    public Resource getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath){
        this.uploadPath = new DefaultResourceLoader().getResource(uploadPath);
    }
}
