package com.example.controller;

/**
 * Created by epakarb on 2016-09-01.
 */

import com.example.profile.UserProfileSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class HomeController {
    private final UserProfileSession userProfileSession;

    @Autowired
    public  HomeController(UserProfileSession userProfileSession){
        this.userProfileSession = userProfileSession;
    }

    @RequestMapping("/")
    public String home(){
        List<String> tastes = userProfileSession.getTastes();
        if(tastes.isEmpty()){
            return "redirest:/profile/profilePage";
        }
        return "redirect:/search/mixed;keywords=" + String.join(",", tastes);
    }
}
