package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by epakarb on 8/2/2016.
 */

@Controller
public class HelloController {
    @RequestMapping("/hello")
//    @ResponseBody
    public String hello(@RequestParam("name") String userName, Model model){
        model.addAttribute("message", "Witaj " + userName + " w spring");
        return "resultPage";
    }
}
